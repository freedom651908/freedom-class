# FREEDOM! through Compliance

Automated SBOMs with GitLab  
Rocky Mountain Cyber 2023  

In this training session, participants will learn how to quickly add security scanning and Software Bill of Materials (SBOM) generation to an existing software project.

Please review the [issues](../../issues) for step-by-step instructions.