import express from "express"
import dotenv from "dotenv"
import bodyParser from "body-parser"

import * as home from "./controllers/home.controller"

dotenv.config()

const app = express()

app.set("port", process.env.PORT || 3000)

const path = process.env.NODE_ENV === 'production'
    ? __dirname + '/../../web/dist/'
    : __dirname + '/../web/dist/'

app.use(express.static(path))
app.get('/', (req, res) => {
    res.sendFile(path + "index.html")
})

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.get("/api/", home.index)

app.listen(app.get("port"), () => {
    console.log(
        "App is running at http://localhost:%d in %s mode",
        app.get("port"),
        app.get("env")
    )
    console.log("Press CTRL-C to stop\n");
})

export default app
