import { Request, Response } from "express"

export const index = (req: Request, res: Response) => {
    res.json({
        message: "Automate SBOM generation and Compliance workflows with GitLab"
    })
}
